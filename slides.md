title: Einstieg ins Forschungsdatenmanagement mit git und GitLab
layout: true
name: nfdi4ing-template
class: nfdi4ing
<div class="slide-layout">
    <div class="slide-header"></div>
    <div class="slide-footer">21.09.2021 — Seite</div>
    <a class="slide-license" href="https://creativecommons.org/licenses/by-sa/4.0" target="_blank" title="CC-BY-SA 4.0"></a>
</div>
---
class: title-red

# Grundlagen der Versionsverwaltung mit git und GitLab

Marius Politze, RWTH Aachen University

Einstieg ins Forschungsdatenmanagement mit git und GitLab

21.09.2021 12:00 - 17:00

---
class: sparse

# Setting The Stage

## Kurze Vorstellungsrunde

* Name, Institution, Tätigkeit
* Ein Punkt, den ich über `git` wissen möchte
* Eine HomeOffice-Beichte / -Anekdote
* ... das Wort weitergeben

---
class: sparse

# Ergebnissicherung der Online-Vorbereitung

* Flipped Classroom Inhalte in GitLab

> https://gitlab-nrw-workshop-2021-09.gitlab.io/preparation/

* Rückfragen zu den Inhalten?
* Ideenspeicher für Fragen

> https://miro.com/app/board/o9J_l1ta3sw=/?moveToWidget=3074457362682111842

* Fehler bei der Installation? Abhilfe für Heute

> https://repl.it/@mpolitze/GitLab-NRW-Workshop

---


# Git: Remote Version Control

.center[
  ![Remote Version Control](img/remote-version-control.svg)
]


---


# Demo: Git Ablauf (2)

.center[
  ![Git Workflow](img/git-workflow.svg)
]

---
class: wide

.center[
  [![Explain git with D3](img/explaingitwithd3.png)](http://onlywei.github.io/explain-git-with-d3/)
]


---

# Git Grundlegende Befehle - *Globale Konfiguration*

## Konfiguration 1x pro Computer erforderlich:

Konfiguriert `<Benutername>` als globalen Git-Benutzernamen
```bash
git config --global user.name <Benutzername>
# z.B.
git config --global user.name "Marius Politze"
```

Konfiguriert `<Email-Adresse>` als globale Git-Emailadresse
```bash
git config --global user.email <Email-Adresse>
# z.B.
git config --global user.email "politze@itc.rwth-aachen.de"
```

---
class: small, wide, cols-2

# Git Grundlegende Befehle - *Interaktion mit dem Repository*

Initialisiert im aktuellen Verzeichnis ein git Repository
```bash
git init
# z.B.
git init
```

Fügt Dateien dem staging Bereich hinzu
```bash
git add
# z.B.
git add .
```

Commitet die Dateien aus dem staging Bereich ins Repository
```bash
git commit -m <commit message>
#z.B.
git commit –m "Erster Commit"
```

<div class="col-break"></div>

Klont ein entferntes Repository
```bash
git clone <repository url>
# z.B.
git clone https://git.rwth-aachen.de/grp/repo.git
```

Versionshistorie an das entfernte Repository senden
```bash
git push <repository url> <branch>
# z.B.
git push https://git.rwth-aachen.de/grp/repo.git master
# oder den aktuellen branch zum default remote
git push
```

Holt den Versionsstand aus dem entfernten Repository und mergt in das Arbeitsverzeichnis
```bash
git pull <repository url> <branch>
# z.B.
git pull https://git.rwth-aachen.de/grp/repo.git master
# oder den aktuellen branch vom default remote
git pull
```

---
class: title-green

# Übung: Git Tac Toe


---
class: img-right

# Übung: Git Tac Toe - Regeln

![Tic Tac Toe](img/tictactoe.svg)

* 2 Spieler: `X` und `O`
* Auf einem Feld 3x3
* Spieler ziehen abwechselnd
* In jedem Zug setzt der Spieler sein Symbol in ein Feld
* Gewonnen hat, wer drei Symbole in einer Zeile, Spalte oder Diagonale hat

In einer Textdatei sieht das Spielfeld dann so aus:

```python
 | | 
-+-+-
 | | 
-+-+-
 | | 
```

---

# Übung: Git Tac Toe - *Runde 1 Abwechselnd (1)*

Vorbereitung: Teilen Sie sich in 2er Gruppen mit `X` und `O`

> `X` erstellt ein GitLab Repository und initialisiert das Projekt mit einer Readme.md

> `X` lädt `O` als *Maintainer* in das Projekt ein

Vorbereitung: Beide Spieler klonen das Repository auf ihren Rechnern

Vorbereitung: `X` legt eine `Runde1.txt` mit dem Spielfeld an

```bash
X> git add Runde1.txt
X> git commit -m "Spielfeld für Runde 1"
```

---
class: dense

# Übung: Git Tac Toe - *Runde 1 Abweschselnd (2)* 

Spielzug: `X` macht den ersten Zug im Spielfeld
```bash
X> git add Runde1.txt
X> git commit -m "X Zug 1"
X> git push
```

Spielzug: `O` aktualisiert das Repository und macht den nächsten Zug

```bash
O> git pull
```

> `O` macht den Zug im Spielfeld

```bash
O> git add Runde1.txt
O> git commit -m "O Zug 1"
O> git push
```
Spielzug: `X` ist an der Reihe und startet mit git pull und dem zweiten Zug

... und so weiter ...

---
class: dense

# Übung: Git Tac Toe - *Runde 2 Gleichzeitig (1)*

Vorbereitung: `X` erstellt eine neue Datei `Runde2.txt`, füllt diese mit dem Spielfeld

```bash
X> git add Runde2.txt
X> git commit -m "Spielfeld für Runde 2"
X> git push
```

Spielzug gleichzeitig: `X` und `O` machen ihre Züge

```bash
X&O> git pull
```

> Zug im Spielfeld eintragen

```bash
X&0> git add Runde2.txt
X&O> git commit –m "{X,O} Zug 1"
```

---
class: dense

# Übung: Git Tac Toe - *Runde 2 Gleichzeitig (2)*

Spielzug nacheinander: Änderungen an den Server senden (Erst Spieler `O`!)

```bash
X&O> git push
```

> *Der push von X wird rejected!*

`X` muss den Konflikt lösen:

```bash
X> git pull
```

> Entweder: git macht einen automatischen merge
> Oder: `X` öffnet `Runde2.txt` und führt die Spielstände zusammen.

```bash
X> git add Runde2.txt
X> git commit -m "Zug 1 zusammengeführt"
X> git push
```
... und so weiter ... 

Machen Sie noch eine Runde 3 mit getauschten Rollen `X` und `O`

---
class: dense

# Übung: Git Tac Toe - *Runde 3 Branches (1)*

Vorbereitung: `X` erstellt eine neue Datei `Runde3.txt` und füllt diese mit dem Spielfeld

```bash
X> git add Runde4.txt
X> git commit -m "Spielfeld für Runde 3"
X> git push
```

Vorbereitung: `O` aktualisiert das Repository: 

```bash
O> git pull
```

Vorbereitung: Beide erstellen einen Branch für ihre Spielzüge:

```bash
X&O> git checkout -b "r4s{X,O}"
X&O> git push --set-upstream origin r4s{X,O}
```

`X` und `O` spielen jeweils auf dem eigenen Branch.

---
class: dense

# Übung: Git Tac Toe - *Runde 3 Branches (2)*

Spielzug gleichzeitig: `X` und `O` machen den Spielzug und senden Änderungen an den Server

> Branches aktualisieren

```bash
X&O> git pull
```

> Zug im Spielfeld eintragen

```bash
X&O> git add Runde4.txt
X&O> git commit –m "{X,O} Zug 1"
X&O> git push
```

---
class: dense

# Übung: Git Tac Toe - *Runde 3 Branches (3)*

Spielzug `X`: Führt die Änderungen zusammen

```bash
git fetch
git merge origin/r4sO
```

> `X` führt Spielstände zusammen

```bash
git add Runde4.txt
git commit -m "Zug 1 zusammengeführt"
git push
```

Spielzug `O`: Holt Änderungen von `X`

```bash
git fetch
git merge origin/r4sX
```

... und so weiter ...

Machen Sie noch eine Runde mit getauschten Rollen `X` und `O`

---
class: wide

<p style="margin-top:-3em">
.center[
![Git Cheat Sheet by by Hylke Bons based on work by Zack Rusin and Sébastien Pierre. This work is licensed under the Creative Commons Attribution 3.0 License.](https://raw.githubusercontent.com/hbons/git-cheat-sheet/master/preview.png)
]

---

class: dense 

# GitLab Walkthrough: *Wiki*

## Zweck: Dokumentation der Daten im Repository
* Sind selbst ein Repository
* Seiten können verlinkt werden
* Markdown ermöglicht Formatierung
* Bilder, Videos und Quellcode können eingefügt werden


--
## Demo: Wikis in GitLab
* Erstellen von Seiten
* Bearbeiten von Seiten
* Markdown:
  * Überschriften
  * Formatierung
  * Links
  * Listen
  * Quellcode
  * Bilder und Videos

---
class: dense

# GitLab Walthrough: *Issues*

## Zweck: Projektmanagement direkt im Repository
* Einzelne Arbeitspakete
* Lassen sich Projektmitgliedern zuweisen
* Lassen sich zu Meilensteinen zusammenfassen
* Verschiedene Ansichten


--
## Demo: Issues in GitLab
* Erstellen von Issues
  * Beschreibung
  * Labels
  * Metadaten
* Arbeitspakete Planen
  * Mit dem Meilensteinen
  * Mit Boards
Mit dem Repository verknüpfen
  * Mit Commits
  * Mit Branches
  * Mit Merge Requests


---
class: sparse, img-right

# Wrap Up

![Feedback Starfish](img/starfish.svg)

* Ideenspeicher

> https://miro.com/app/board/o9J_l1ta3sw=/?moveToWidget=3074457362682111842

* Fragen?
* Feedback Seestern

> https://miro.com/app/board/o9J_l1ta3sw=/?moveToWidget=3074457362682111898
